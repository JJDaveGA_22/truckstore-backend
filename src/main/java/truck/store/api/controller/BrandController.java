package truck.store.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import truck.store.api.response.BrandResponse;
import truck.store.api.service.BrandService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/brand")
public class BrandController {
	
	@Autowired
	BrandService brandService;
	
	@GetMapping("/getById/{id}")
	public BrandResponse getById(@PathVariable long id) {
		return brandService.getById(id);
	}

}
