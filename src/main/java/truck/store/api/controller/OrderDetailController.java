package truck.store.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import truck.store.api.request.CreateOrderDetailRequest;
import truck.store.api.request.CreateOrderRequest;
import truck.store.api.response.OrderDetailResponse;
import truck.store.api.response.OrderResponse;
import truck.store.api.service.OrderDetailService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/order/detail")
public class OrderDetailController {

	@Autowired
	OrderDetailService orderDetailService;
	
	@GetMapping("/getById/{id}")
	public OrderDetailResponse getById(@PathVariable long id) {
		return orderDetailService.getById(id);
	}
	
	@GetMapping("/getAll")
	public List<OrderDetailResponse> getAll() {
		return orderDetailService.getAll();
	}
	
	@GetMapping("/getByOrderId/{orderId}")
	public List<OrderDetailResponse> getByOrderId(@PathVariable long orderId) {
		return orderDetailService.getByOrderId(orderId);
	}

	@PostMapping("/create")
	public List<OrderDetailResponse> create(@RequestBody List<CreateOrderDetailRequest> createOrderDetailRequest) {
		return orderDetailService.create(createOrderDetailRequest);
	}
}
