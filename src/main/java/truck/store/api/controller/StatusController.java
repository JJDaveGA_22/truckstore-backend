package truck.store.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import truck.store.api.response.StatusResponse;
import truck.store.api.service.StatusService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/status")
public class StatusController {

	@Autowired
	StatusService statusService;
	
	@GetMapping("/getById/{id}")
	public StatusResponse getById(@PathVariable long id) {
		return statusService.getById(id);
	}
	
	@GetMapping("/getAll")
	public List<StatusResponse> getAll() {
		return statusService.getAll();
	}
}
