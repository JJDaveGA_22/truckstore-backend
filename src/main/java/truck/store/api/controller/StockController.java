package truck.store.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import truck.store.api.response.StockResponse;
import truck.store.api.service.StockService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/stock")
public class StockController {

	@Autowired
	StockService stockService;
	
	@GetMapping("/getById/{id}")
	public StockResponse getById(@PathVariable long id) {
		return stockService.getById(id);
	}
	
	@GetMapping("/getAll")
	public List<StockResponse> getAll() {
		return stockService.getAll();
	}
	
}
