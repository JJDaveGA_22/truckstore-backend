package truck.store.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import truck.store.api.response.TruckResponse;
import truck.store.api.service.TruckService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/truck")
public class TruckController {
	
	@Autowired
	TruckService truckService;
	
	@GetMapping("/getById/{id}")
	public TruckResponse getById(@PathVariable long id) {
		return truckService.getById(id);
	}
	
	@GetMapping("/getAll")
	public List<TruckResponse> getAll() {
		return truckService.getAll();
	}

}
