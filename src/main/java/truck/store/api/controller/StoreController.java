package truck.store.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import truck.store.api.response.StoreResponse;
import truck.store.api.service.StoreService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/store")
public class StoreController {

	@Autowired
	StoreService storeService;
	
	@GetMapping("/getById/{id}")
	public StoreResponse getById(@PathVariable long id) {
		return storeService.getById(id);
	}
	
	@GetMapping("/getAll")
	public List<StoreResponse> getAll() {
		return storeService.getAll();
	}
	
}
