package truck.store.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import truck.store.api.request.LoginRequest;
import truck.store.api.response.UserResponse;
import truck.store.api.service.UserService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@GetMapping("/getById/{id}")
	public UserResponse getById(@PathVariable long id) {
		return userService.getById(id);
	}
	
	@PostMapping("/login")
	public UserResponse login(@RequestBody LoginRequest loginRequest) {
		return userService.login(loginRequest);
	}

}
