package truck.store.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import truck.store.api.request.CreateOrderRequest;
import truck.store.api.request.UpdateOrderRequest;
import truck.store.api.response.OrderResponse;
import truck.store.api.service.OrderService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/order")
public class OrderController {

	@Autowired
	OrderService orderService;
	
	@GetMapping("/getById/{id}")
	public OrderResponse getById(@PathVariable long id) {
		return orderService.getById(id);
	}
	
	@GetMapping("/getAll")
	public List<OrderResponse> getAll() {
		return orderService.getAll();
	}
	
	@PostMapping("/update")
	public OrderResponse create(@RequestBody UpdateOrderRequest updateOrderRequest) {
		return orderService.update(updateOrderRequest);
	}
	
	@PostMapping("/create")
	public OrderResponse create(@RequestBody CreateOrderRequest createOrderRequest) {
		return orderService.create(createOrderRequest);
	}
	
}
