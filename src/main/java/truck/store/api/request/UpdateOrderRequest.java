package truck.store.api.request;

import java.util.Date;

import truck.store.api.entity.Status;
import truck.store.api.entity.User;

public class UpdateOrderRequest {

	private Long id;
	
	private Date lastOrderUpdate;
	
	private String clientName;
    
	private String clientAddress;
	
	private Date orderDate;
    
	private User soldBy;
	
	private Integer quantity;
	
	private Double discount;

	private Double total;
	
	private Status status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getLastOrderUpdate() {
		return lastOrderUpdate;
	}

	public void setLastOrderUpdate(Date lastOrderUpdate) {
		this.lastOrderUpdate = lastOrderUpdate;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientAddress() {
		return clientAddress;
	}

	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public User getSoldBy() {
		return soldBy;
	}

	public void setSoldBy(User soldBy) {
		this.soldBy = soldBy;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
}
