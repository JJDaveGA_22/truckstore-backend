package truck.store.api.request;

import java.sql.Date;

import truck.store.api.entity.Store;
import truck.store.api.entity.Truck;

public class CreateOrderDetailRequest {

	private Long orderId;
	
    private Truck truck;
	
    private Store store;
    
	private Integer quantity;
	
	private Double uniquePrice;
	
	private Double discount;

	private Double total;
	
	private Date orderDate;
	
	private Long status;
	
	public CreateOrderDetailRequest() {}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Truck getTruck() {
		return truck;
	}

	public void setTruck(Truck truck) {
		this.truck = truck;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getUniquePrice() {
		return uniquePrice;
	}

	public void setUniquePrice(Double uniquePrice) {
		this.uniquePrice = uniquePrice;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

}
