package truck.store.api.entity;

import java.util.Date;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "TruckOrder")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private Long id;
	
	@Column(name = "OrderDate", nullable = true, columnDefinition = "DATETIME DEFAULT GETDATE()")
	private Date orderDate;
	
    @Column(name = "LastOrderUpdate")
	private Date lastOrderUpdate;
    
    @Column(name = "ClientName")
	private String clientName;
    
    @Column(name = "ClientAddress")
	private String clientAddress;
    
	// @Column(name = "SoldById")
	// private Integer soldById;
	@OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "soldById", referencedColumnName = "Id")
    private User soldBy;
	
	@Column(name = "Quantity")
	private Integer quantity;
	
	@Column(name = "Discount")
	private Double discount;

	@Column(name = "Total")
	private Double total;
	
	// @Column(name = "Status")
	// private Long status;
	@OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "status", referencedColumnName = "Id")
    private Status status;
	
	public Order() {}
	
	public Order(Order order) {
		this.id = order.getId();
		this.orderDate = order.getOrderDate();
		this.lastOrderUpdate = order.getLastOrderUpdate();
		this.clientName = order.getClientName();
		this.clientAddress = order.getClientAddress();
		this.soldBy = order.getSoldBy();
		this.quantity = order.getQuantity();
		this.discount = order.getDiscount();
		this.total = order.getTotal();
		this.status = order.getStatus();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getLastOrderUpdate() {
		return lastOrderUpdate;
	}

	public void setLastOrderUpdate(Date lastOrderUpdate) {
		this.lastOrderUpdate = lastOrderUpdate;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientAddress() {
		return clientAddress;
	}

	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}

	public User getSoldBy() {
		return soldBy;
	}

	public void setSoldBy(User soldBy) {
		this.soldBy = soldBy;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
