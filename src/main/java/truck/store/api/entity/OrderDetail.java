package truck.store.api.entity;

import java.sql.Date;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "TruckOrderDetail")
public class OrderDetail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Id")
	private Long id;
	
	@Column(name = "OrderId")
	private Long orderId;
	
	/*@Column(name = "TruckId")
    private Long truckId;*/
	
	@OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "TruckId", referencedColumnName = "Id")
    private Truck truck;
	
	/*@Column(name = "StoreId")
    private Long storeId;*/
	
	@OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "StoreId", referencedColumnName = "Id")
    private Store store;
    
	@Column(name = "Quantity")
	private Integer quantity;
	
	@Column(name = "UniquePrice")
	private Double uniquePrice;
	
	@Column(name = "Discount")
	private Double discount;

	@Column(name = "Total")
	private Double total;
	
	@Column(name = "OrderDate")
	private Date orderDate;
	
	@Column(name = "Status")
	private Long status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	/*public Long getTruckId() {
		return truckId;
	}

	public void setTruckId(Long truckId) {
		this.truckId = truckId;
	}*/

	public Truck getTruck() {
		return truck;
	}

	public void setTruck(Truck truck) {
		this.truck = truck;
	}

	/*public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}*/

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getUniquePrice() {
		return uniquePrice;
	}

	public void setUniquePrice(Double uniquePrice) {
		this.uniquePrice = uniquePrice;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

}
