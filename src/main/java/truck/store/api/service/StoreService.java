package truck.store.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import truck.store.api.entity.Store;
import truck.store.api.repository.StoreRepository;
import truck.store.api.response.StoreResponse;

@Service
public class StoreService {
	
	@Autowired
	StoreRepository storeRepository;
	
	public StoreResponse getById(long id) {
		Store store = storeRepository.findById(id).get();
		StoreResponse storeResponse = new StoreResponse(store);
		return storeResponse;
	}
	
	public List<StoreResponse> getAll() {
		List<Store> stores = storeRepository.findAll();
		ArrayList<StoreResponse> storeResponse = new ArrayList<StoreResponse>();
		stores.forEach((s) -> {
			storeResponse.add(new StoreResponse(s));
		});
		return storeResponse;
	}

}
