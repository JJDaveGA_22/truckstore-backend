package truck.store.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import truck.store.api.entity.Brand;
import truck.store.api.repository.BrandRepository;
import truck.store.api.response.BrandResponse;

@Service
public class BrandService {

	@Autowired
	BrandRepository brandRepository;
	
	public BrandResponse getById(long id) {
		Brand brand = brandRepository.findById(id).get();
		BrandResponse brandResponse = new BrandResponse(brand);
		return brandResponse;
	}
	
}
