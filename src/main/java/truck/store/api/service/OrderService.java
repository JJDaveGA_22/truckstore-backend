package truck.store.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import truck.store.api.entity.Order;
import truck.store.api.repository.OrderRepository;
import truck.store.api.request.CreateOrderRequest;
import truck.store.api.request.UpdateOrderRequest;
import truck.store.api.response.OrderResponse;

@Service
public class OrderService {

	@Autowired
	OrderRepository orderRepository;
	
	public OrderResponse getById(long id) {
		Order order = orderRepository.findById(id).get();
		OrderResponse orderResponse = new OrderResponse(order);
		return orderResponse;
	}
	
	public List<OrderResponse> getAll() {
		List<Order> orders = orderRepository.findAll();
		ArrayList<OrderResponse> orderResponse = new ArrayList<OrderResponse>();
		orders.forEach((s) -> {
			orderResponse.add(new OrderResponse(s));
		});
		return orderResponse;
	}
	
	public OrderResponse update(UpdateOrderRequest updateOrderRequest) {
		Order order = new Order();
		order.setId(updateOrderRequest.getId());
		order.setClientName(updateOrderRequest.getClientName());
		order.setClientAddress(updateOrderRequest.getClientAddress());
		order.setOrderDate(updateOrderRequest.getOrderDate());
		order.setSoldBy(updateOrderRequest.getSoldBy());
		order.setQuantity(updateOrderRequest.getQuantity());
		order.setDiscount(updateOrderRequest.getDiscount());
		order.setTotal(updateOrderRequest.getTotal());
		order.setStatus(updateOrderRequest.getStatus());
		order.setLastOrderUpdate(updateOrderRequest.getLastOrderUpdate());
		order = orderRepository.save(order);
		return new OrderResponse(order);
	}
	
	public OrderResponse create(CreateOrderRequest createOrderRequest) {
		Order order = new Order();
		order.setClientName(createOrderRequest.getClientName());
		order.setClientAddress(createOrderRequest.getClientAddress());
		order.setOrderDate(createOrderRequest.getOrderDate());
		order.setSoldBy(createOrderRequest.getSoldBy());
		order.setQuantity(createOrderRequest.getQuantity());
		order.setDiscount(createOrderRequest.getDiscount());
		order.setTotal(createOrderRequest.getTotal());
		order.setStatus(createOrderRequest.getStatus());
		order = orderRepository.save(order);
		return new OrderResponse(order);
	}
	
}