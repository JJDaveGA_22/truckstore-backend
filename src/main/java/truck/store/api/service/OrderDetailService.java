package truck.store.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import truck.store.api.entity.Order;
import truck.store.api.entity.OrderDetail;
import truck.store.api.repository.OrderDetailRepository;
import truck.store.api.request.CreateOrderDetailRequest;
import truck.store.api.request.CreateOrderRequest;
import truck.store.api.response.OrderDetailResponse;
import truck.store.api.response.OrderResponse;

@Service
public class OrderDetailService {

	@Autowired
	OrderDetailRepository orderDetailRepository;
	
	public OrderDetailResponse getById(long id) {
		OrderDetail orderDetail = orderDetailRepository.findById(id).get();
		OrderDetailResponse orderDetailResponse = new OrderDetailResponse(orderDetail);
		return orderDetailResponse;
	}
	
	public List<OrderDetailResponse> getAll() {
		List<OrderDetail> orderDetails = orderDetailRepository.findAll();
		ArrayList<OrderDetailResponse> orderDetailResponse = new ArrayList<OrderDetailResponse>();
		orderDetails.forEach((s) -> {
			orderDetailResponse.add(new OrderDetailResponse(s));
		});
		return orderDetailResponse;
	}
	
	public List<OrderDetailResponse> getByOrderId(long orderId) {
		List<OrderDetail> orderDetails = orderDetailRepository.findByOrderId(orderId);
		ArrayList<OrderDetailResponse> orderDetailResponse = new ArrayList<OrderDetailResponse>();
		orderDetails.forEach((s) -> {
			orderDetailResponse.add(new OrderDetailResponse(s));
		});
		return orderDetailResponse;
	}
	
	public List<OrderDetailResponse> create(List<CreateOrderDetailRequest> createOrderRequest) {
		List orders = new ArrayList<OrderDetail>();
		List response = new ArrayList<OrderDetailResponse>();
		createOrderRequest.forEach((od) -> {
			OrderDetail orderDetail = new OrderDetail();
			orderDetail.setOrderId(od.getOrderId());
			orderDetail.setTruck(od.getTruck());
			orderDetail.setStore(od.getStore());
			orderDetail.setQuantity(od.getQuantity());
			orderDetail.setUniquePrice(od.getUniquePrice());
			orderDetail.setDiscount(od.getDiscount());
			orderDetail.setTotal(od.getTotal());
			orderDetail.setOrderDate(od.getOrderDate());
			orderDetail.setStatus(od.getStatus());
			orderDetail = orderDetailRepository.save(orderDetail);
			response.add(new OrderDetailResponse(orderDetail));
		});
		return response;
	}
	
}
