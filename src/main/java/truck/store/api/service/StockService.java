package truck.store.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import truck.store.api.entity.Stock;
import truck.store.api.repository.StockRepository;
import truck.store.api.response.StockResponse;

@Service
public class StockService {

	@Autowired
	StockRepository stockRepository;
	
	public StockResponse getById(long id) {
		Stock stock = stockRepository.findById(id).get();
		StockResponse stockResponse = new StockResponse(stock);
		return stockResponse;
	}
	
	public List<StockResponse> getAll() {
		List<Stock> stocks = stockRepository.findAll();
		ArrayList<StockResponse> stockResponse = new ArrayList<StockResponse>();
		stocks.forEach((s) -> {
			stockResponse.add(new StockResponse(s));
		});
		return stockResponse;
	}
	
}
