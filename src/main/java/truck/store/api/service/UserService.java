package truck.store.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import truck.store.api.entity.User;
import truck.store.api.repository.UserRepository;
import truck.store.api.request.LoginRequest;
import truck.store.api.response.UserResponse;

@Service
public class UserService {
	
	@Autowired
	UserRepository userRepository;
	
	public UserResponse getById(long id) {
		User user = userRepository.findById(id).get();
		UserResponse userResponse = new UserResponse(user);
		return userResponse;
	}
	
	public UserResponse login(LoginRequest loginRequest) {
		User user = userRepository.findByUsername(loginRequest.getUsername());
		UserResponse userResponse = new UserResponse(user);
		return userResponse;
	}

}
