package truck.store.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import truck.store.api.entity.Status;
import truck.store.api.repository.StatusRepository;
import truck.store.api.response.StatusResponse;


@Service
public class StatusService {

	@Autowired
	StatusRepository statusRepository;
	
	public StatusResponse getById(long id) {
		Status status = statusRepository.findById(id).get();
		StatusResponse statusResponse = new StatusResponse(status);
		return statusResponse;
	}
	
	public List<StatusResponse> getAll() {
		List<Status> sts = statusRepository.findAll();
		ArrayList<StatusResponse> stsResponse = new ArrayList<StatusResponse>();
		sts.forEach((s) -> {
			stsResponse.add(new StatusResponse(s));
		});
		return stsResponse;
	}
	
}
