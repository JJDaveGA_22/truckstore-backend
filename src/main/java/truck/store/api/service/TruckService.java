package truck.store.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import truck.store.api.entity.Truck;
import truck.store.api.repository.TruckRepository;
import truck.store.api.response.TruckResponse;

@Service
public class TruckService {

	@Autowired
	TruckRepository truckRepository;
	
	public TruckResponse getById(long id) {
		Truck truck = truckRepository.findById(id).get();
		TruckResponse truckResponse = new TruckResponse(truck);
		return truckResponse;
	}
	
	public List<TruckResponse> getAll() {
		List<Truck> trucks = truckRepository.findAll();
		ArrayList<TruckResponse> trucksResponse = new ArrayList<TruckResponse>();
		trucks.forEach((t) -> {
			trucksResponse.add(new TruckResponse(t));
		});
		return trucksResponse;
	}
	
}
