package truck.store.api.response;

import truck.store.api.entity.Brand;

public class BrandResponse {
	
	private long id;
	
	private String name;
	
	public BrandResponse(Brand brand) {
		this.id = brand.getId();
		this.name = brand.getName();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

}
