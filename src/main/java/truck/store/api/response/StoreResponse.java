package truck.store.api.response;

import java.sql.Date;

import truck.store.api.entity.Store;


public class StoreResponse {

	private Long id;
	
	private String name;
	
	private String address;
	
	private Date createdAt;
	
	private Long createdBy;

	private Date updatedAt;

	private Long updatedBy;
	
	private Long status;
	
	public StoreResponse(Store store) {
		this.id = store.getId();
		this.name = store.getName();
		this.address = store.getAddress();
		this.createdAt = store.getCreatedAt();
		this.createdBy = store.getCreatedBy();
		this.updatedAt = store.getUpdatedAt();
		this.updatedBy = store.getUpdatedBy();
		this.status = store.getStatus();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}
	
}
