package truck.store.api.response;

import truck.store.api.entity.Status;

public class StatusResponse {

	private long id;
	
	private String name;
	
	public StatusResponse(Status status) {
		this.id = status.getId();
		this.name = status.getName();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
