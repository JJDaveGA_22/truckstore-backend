package truck.store.api.response;

import java.util.Date;

import truck.store.api.entity.Order;
import truck.store.api.entity.Status;
import truck.store.api.entity.User;

public class OrderResponse {

	private Long id;
	
	private Date orderDate;
	
	private Date lastOrderUpdate;
    
	private String clientName;
    
	private String clientAddress;
    
	private User soldBy;
	
	private Integer quantity;
	
	private Double discount;

	private Double total;
	
	private Status status;
	
	public OrderResponse(Order order) {
		this.id = order.getId();
		this.orderDate = order.getOrderDate();
		this.lastOrderUpdate = order.getLastOrderUpdate();
		this.clientName = order.getClientName();
		this.clientAddress = order.getClientAddress();
		this.soldBy = order.getSoldBy();
		this.quantity = order.getQuantity();
		this.discount = order.getDiscount();
		this.total = order.getTotal();
		this.status = order.getStatus();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getLastOrderUpdate() {
		return lastOrderUpdate;
	}

	public void setLastOrderUpdate(Date lastOrderUpdate) {
		this.lastOrderUpdate = lastOrderUpdate;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientAddress() {
		return clientAddress;
	}

	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}

	public User getSoldBy() {
		return soldBy;
	}

	public void setSoldBy(User soldBy) {
		this.soldBy = soldBy;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
}
