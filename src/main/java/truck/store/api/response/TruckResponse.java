package truck.store.api.response;

import java.sql.Date;

import truck.store.api.entity.Brand;
import truck.store.api.entity.Truck;

public class TruckResponse {
	
	private Long id;
	
	private String name;
	
	private String description;
	
    private Brand brand;
    
	private String model;
    
	private Date createdAt;
	
	private Long createdBy;
	
	private Date updatedAt;

	private Long updatedBy;
	
	private Long status;
	
	public TruckResponse(Truck truck) {
		this.id = truck.getId();
		this.name = truck.getName();
		this.description = truck.getDescription();
		this.brand = truck.getBrand();
		this.model = truck.getModel();
		this.createdAt = truck.getCreatedAt();
		this.createdBy = truck.getCreatedBy();
		this.updatedAt = truck.getUpdatedAt();
		this.updatedBy = truck.getUpdatedBy();
		this.status = truck.getStatus();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

}
