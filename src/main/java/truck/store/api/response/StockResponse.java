package truck.store.api.response;

import java.sql.Date;

import truck.store.api.entity.Stock;
import truck.store.api.entity.Store;
import truck.store.api.entity.Truck;

public class StockResponse {
	
	private Long id;
	
    private Truck truck;
	
    private Store store;
	
	private Double price;
	
	private Double discount;
	
	private Integer available;
	
	private Integer sold;
	
	private Date createdAt;
	
	private Long createdBy;
	
	private Date updatedAt;

	private Long updatedBy;
	
	private Long status;
	
	public StockResponse(Stock stock) {
		this.id = stock.getId();
		this.truck = stock.getTruck();
		this.store = stock.getStore();
		this.price = stock.getPrice();
		this.discount = stock.getDiscount();
		this.available = stock.getAvailable();
		this.createdAt = stock.getCreatedAt();
		this.createdBy = stock.getCreatedBy();
		this.updatedAt = stock.getUpdatedAt();
		this.updatedBy = stock.getUpdatedBy();
		this.status = stock.getStatus();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Truck getTruck() {
		return truck;
	}

	public void setTruck(Truck truck) {
		this.truck = truck;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}

	public Integer getSold() {
		return sold;
	}

	public void setSold(Integer sold) {
		this.sold = sold;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

}
