package truck.store.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import truck.store.api.entity.Status;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {

}