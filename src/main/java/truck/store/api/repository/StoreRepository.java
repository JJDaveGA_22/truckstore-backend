package truck.store.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import truck.store.api.entity.Store;

@Repository
public interface StoreRepository extends JpaRepository<Store, Long> {
	
}